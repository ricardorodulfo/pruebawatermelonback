var express = require('express');
var session = require('express-session');
let cors = require('cors');
let fetch = require('node-fetch');
let redis = require("redis");
let redisClient = redis.createClient(6379);
let RedisStore = require('connect-redis')(session);
const { base64encode, base64decode } = require('nodejs-base64');

var app = express();

var sessionMiddleware = session({
    store: new RedisStore({ client: redisClient }),
    secret: "clave",
    cookie: {
        maxAge: 120000
    }
});

app.use(cors());
app.use(sessionMiddleware);

app.use(function(req, res, next) {
    if(!req.session.views)
    {
        if(req.baseUrl == "/iniciar-sesion" || req.baseUrl == "/registro")
        {
            next();
        }

        res.status(440).json({error: 'Sin sesion'});
    } else {
        req.session.views++;
        next();
    }
});

app.use(express.json());

app.post('/register', function (req, res) {

    if(req.body.usuario && req.body.contrasenia)
    {
        redisClient.set(`${req.body.usuario}`, base64encode(`${req.body.contrasenia}`));        
    }

    return res.status(200).send({ user: req.body.usuario });
});

app.get('/datos', function (req, res) {

    fetch("https://rickandmortyapi.com/api/character/")
        .then(response => {
            return response.json();
        })
        .then(response => {
            return res.json({ data: JSON.stringify(response) }).end();
        })
        .catch(error => {
            return res.status(500).json(error.toString()).end();
        });

});

app.post('/login', function (req, res) {

    if(req.body.usuario && req.body.contrasenia)
    {
        redisClient.get(`${req.body.usuario}`, function (err, response) {

            if(!response)
            {
                return res.status(401).json({error: "Usuario no encontrado"}).end();
            } else {

                if(base64decode(response) == req.body.contrasenia)
                {
                    res.session.views++;
                    return res.json({ user: req.body.usuario }).end();                    
                }

                return res.status(401).json({error: "Contrasenia incorrecta"}).end();                
            }            
            
        });
    } else {
        return res.status(401).json({error: "Usuario no encontrado"}).end();
    }

});

app.listen(8080, () => {
    console.log('Servidor escuchando en puerto ', 8080);
});